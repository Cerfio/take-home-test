export class Drug {
  constructor(name, expiresIn, benefit) {
    this.name = name;
    this.expiresIn = expiresIn;
    this.benefit = benefit;
  }
}

export class Pharmacy {
  constructor(drugs = []) {
    this.drugs = drugs;
  }

  updateBenefitValue() {
    this.drugs.forEach(drug => {
      switch (drug.name) {
        case "Herbal Tea":
          this.updateHerbalTea(drug);
          break;
        case "Fervex":
          this.updateFervex(drug);
          break;
        case "Magic Pill":
          // Do nothing
          break;
        case "Dafalgan":
          this.updateDafalgan(drug);
          break;
        default:
          this.updateNormalDrug(drug);
          break;
      }
      this.decrementExpiresIn(drug);
      this.handleExpired(drug);
    });

    return this.drugs;
  }

  updateHerbalTea(drug) {
    if (drug.benefit < 50) {
      drug.benefit += 1;
    }
  }

  updateFervex(drug) {
    if (drug.benefit < 50) {
      drug.benefit += 1;
      if (drug.expiresIn < 11 && drug.benefit < 50) {
        drug.benefit += 1;
      }
      if (drug.expiresIn < 6 && drug.benefit < 50) {
        drug.benefit += 1;
      }
    }
  }

  updateDafalgan(drug) {
    if (drug.benefit > 0) {
      drug.benefit -= 2;
      if (drug.benefit < 0) {
        drug.benefit = 0;
      }
    }
  }

  updateNormalDrug(drug) {
    if (drug.benefit > 0) {
      drug.benefit -= 1;
    }
  }

  decrementExpiresIn(drug) {
    if (drug.name !== "Magic Pill") {
      drug.expiresIn -= 1;
    }
  }

  handleExpired(drug) {
    if (drug.expiresIn < 0) {
      switch (drug.name) {
        case "Herbal Tea":
          if (drug.benefit < 50) {
            drug.benefit += 1;
          }
          break;
        case "Fervex":
          drug.benefit = 0;
          break;
        case "Magic Pill":
          // Magic Pill does not change
          break;
        default:
          if (drug.benefit > 0) {
            drug.benefit -= 1;
          }
          break;
      }
    }
  }
}
